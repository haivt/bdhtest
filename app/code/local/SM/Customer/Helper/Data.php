<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/4/14
 * Time: 7:30 PM
 */
class SM_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function createNewCustomer($email, $password)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $customerEntity = $resource->getTableName('customer/entity');
        $query = "SELECT * FROM `" . $customerEntity . "` WHERE `email` = '"
            . $email . "'";
        $mageCustomer = $readConnection->fetchAll($query);

        if (count($mageCustomer) === 0) {
            $query = "SELECT * FROM `sm_old_customers` WHERE `email` = '"
                . $email . "'";
            $oldCustomer = $readConnection->fetchAll($query);
            if (count($oldCustomer) != 0) {
                try {
                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
                    $customer->setStoreId(Mage::app()->getStore()->getStoreId());
                    $customer->setFirstname($oldCustomer[0]['first_name']);
                    $customer->setLastname($oldCustomer[0]['last_name']);
                    $customer->setOldId($oldCustomer[0]['id']);
                    $customer->setEmail($email);
                    $customer->setPassword($password);
                    $customer->save();
                } catch (Exception $e) {
                    Mage::log('email - ' . $oldCustomer[0]['email'], null, 'migrate_customer.log');
                    Mage::log('exception - ' . $e->__toString(), null, 'migrate_customer.log');
                }
            }
        }
    }
}