<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/3/14
 * Time: 3:55 PM
 */
try {
    $installer = $this;
    $installer->startSetup();
    $_helper = Mage::helper('sm_setup');
    $cfg = Mage::app()->getConfig();
    $_emailCfg = array(
        array('customer/create_account/email_template','New Account'),
        array('contacts/email/email_template','Contact Form'),
        array('customer/password/forgot_email_template','Forgot Password'),
        array('sales_email/invoice/template','New Invoice'),
        array('sales_email/order/template','New Order'),
        array('sales_email/shipment/template','New Shipment'),
        array('sales_email/invoice/guest_template','New Invoice for Guest'),
        array('sales_email/order/guest_template','New Order for Guest'),
        array('sales_email/shipment/guest_template','New Shipment for Guest'),
        array('newsletter/subscription/success_email_template','Newsletter subscription success'),
    );
    foreach($_emailCfg as $_email){
        $_emailTempId = $_helper->_getEmailId($_email['1']);
        if($_emailTempId){
            $cfg->saveConfig($_email['0'],$_emailTempId);
        }
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}