<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:04 AM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//-------------------------creat atribute best seller------------------------------
$model = Mage::getModel('eav/entity_setup','core_setup');
$data = array(
    'type' => 'int',
    'group' => 'General',
    'input' => 'boolean',
    'label' => 'Home page best seller',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_required' => '0',
    'is_comparable' => '0',
    'is_searchable' => '0',
    'is_unique' => '0',
    'is_configurable' => '0',
    'use_defined' => '0'

);
$attribute_code = "home_page_best_seller";
$attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
$attribute_id = $attribute->getData('attribute_id');
if($attribute_id == null) {
    $model->addAttribute('catalog_product', 'home_page_best_seller', $data);
}

// Create about-header block
$content_header = <<<EOD
<div class="img-about-header">
    <img class="img-header" src="{{skin url='images/slider/faq_banner.gif'}}" alt="" />
</div>
EOD;

$about_headerBlock = array(
    'title' => 'About Header Block',
    'identifier' => 'about_header_block',
    'content' => $content_header,
    'is_active' => 1,
    'stores' => array(0)
);

$ahBlock = Mage::getModel('cms/block')->load('about_header_block');
if (!$ahBlock->getId()) {
    Mage::getModel('cms/block')->setData($about_headerBlock)->save();
} else {
    $ahBlock->setContent($content_header)->save();
}

// Create about-content block
$content_about = <<<EOD
<div class="container">
	<div class="about-us">
		<h3 class="title-page"><span>ABOUT US</span></h3>
		<p class="intro-about">Your search for the most natural looking hair possible for your weaves and extensions stops at Bella Dream Hair</p>
	</div>
	<div class="about-content">
		<div class="item-left">
			<div class="bx-content">
				<h1>Bella Dream Hair</h1>
				<h2>Natural Human Hair Weaves</h2>
				<p>Bella Dream Hair offers 100% Human Hair Extensions : Mongolian Wavy, Curly, Brazilian Wavy, Natural Wave, Curly, Skin Weft Extensions, Clip on Extensions, Micro Links and Custom Wigs at Affordable Prices. <br /><br /> We believe in offering the highest level of hair and service at affordable prices. All of our hair is made from 100% human hair, comes in natural colors, and can be dyed to achieve any specific hue. Our beautiful hair extensions are crafted from the highest quality human hair and are meticulously put together following natural hair growth patterns, resulting in the silkiest, smoothest hair extensions. <br /><br /> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ipsum sem, accumsan eget varius a, dictum pharetra nibh. Etiam mattis augue ac quam bibendum porttitor. Praesent quis nunc a lorem scelerisque pharetra. Vivamus ut mi neque, tristique viverra odio. Vivamus quam elit, faucibus accumsan lacinia nec, vulputate tempus ligula. Sed et felis libero. Ut dignissim pharetra neque, ac semper eros malesuada sit amet. Aliquam iaculis lectus nec mauris eleifend vel venenatis libero bibendum. Sed sit amet molestie lectus. Donec lectus urna, imperdiet quis lobortis vitae, rhoncus quis est. Maecenas eu risus risus, convallis suscipit sem. Maecenas luctus sagittis rutrum. Quisque elit neque, tincidunt eget consequat laoreet, facilisis et nisi. Sed dignissim lacinia pharetra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
			</div>
		</div>
		<div class="item-right">
			<div class="bx-list">
				<h3>MORE INFORMATION</h3>
				<ul>
				<li><a href="{{store url='frequently-asked-questions'}}"><span class="button_roll_over">Frequently Asked Questions</span></a></li>
				<li><a href="{{store url='testimonials'}}"><span class="button_roll_over">Testimonials</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
EOD;

$content_headerBlock = array(
    'title' => 'About Content Block',
    'identifier' => 'content_header_block',
    'content' => $content_about,
    'is_active' => 1,
    'stores' => array(0)
);

$ahBlock = Mage::getModel('cms/block')->load('content_header_block');
if (!$ahBlock->getId()) {
    Mage::getModel('cms/block')->setData($content_headerBlock)->save();
} else {
    $ahBlock->setContent($content_about)->save();
}


// Create About CMS Page
$content = <<<EOD
<div id="contactImg" class="about-header">{{block type="cms/block" block_id="about_header_block" }}</div>
<div id="about">{{block type="cms/block" block_id="content_header_block" }}</div>
EOD;
$cmsPage = array(
    'title' => 'About',
    'identifier' => 'about',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0),
    'root_template' => 'one_column'
);
$aboutPage = Mage::getModel('cms/page')->load('about');
if(!$aboutPage->getId()){
    Mage::getModel('cms/page')->setData($cmsPage)->save();
}else{
    $aboutPage->setContent($content)->setRootTemplate('one_column')->save();
}

$Config = Mage::app()->getConfig();
$Config ->saveConfig('design/theme/locale', "dreamhair");
$Config ->saveConfig('design/theme/template', "dreamhair");
$Config ->saveConfig('design/theme/skin', "dreamhair");
$Config ->saveConfig('design/theme/layout', "dreamhair");
$installer->endSetup();