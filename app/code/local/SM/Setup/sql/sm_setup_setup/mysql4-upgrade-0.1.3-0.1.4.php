<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:28 AM
 */

try {
    $installer = $this;
    $installer->startSetup();

    // Create attribute bdh_weight & bdh_length
    $weightData = array('4oz', '6oz');
    $lengthData = array('08', '10', '12', '14', '16', '18', '20', '22', '24', '26', '28', '30', '32', '34', '36');

    $newAtrrs = array(
        'bdh_weight'    => array('BDH Weight', $weightData),
        'bdh_length'    => array('BDH Length', $lengthData)
    );
    foreach($newAtrrs as $code => $data){
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, array(
            'input' => 'select',
            'type' => 'varchar',
            'label' => $data[0],
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'backend' => '',
            'visible' => true,
            'required' => true,
            'user_defined' => true,
            'searchable' => true,
            'filterable' => true,
            'comparable' => true,
            'default' => NULL,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'visible_in_advanced_search' => true,
            'is_html_allowed_on_front' => '0',
            'used_in_product_listing' => true,
            'is_configurable' => true,
            'apply_to' => 'simple,grouped,configurable',
            'option' => array(
                'values' => $data[1]
            )
        ));
    }

    //Create attribute set
    $installer->addAttributeSet('catalog_product', 'Base Product');
    $installer->addAttributeSet('catalog_product', 'Hair Zone Product');

    $attributeSetIdBase = $installer->getAttributeSetId('catalog_product', 'Base Product');
    $attributeSetIdHairZone = $installer->getAttributeSetId('catalog_product', 'Hair Zone Product');

    // Create attribute group for two above attribute-set
    $groupArr = array(
        'Prices' => 10,
        'BDH Attributes' => 11,
        'Meta Information' => 12,
        'Images' => 13,
        'Description' => 14,
        'Design' => 15,
        'Gift Options' => 16,
        'Recurring Profile' => 17
    );
    foreach ($groupArr as $name => $sort) {
        $modelGroup = Mage::getModel('eav/entity_attribute_group');
        $modelGroup->setAttributeGroupName($name)
            ->setAttributeSetId($attributeSetIdBase)
            ->setSortOrder($sort);
        $modelGroup->save();

        $modelGroup = Mage::getModel('eav/entity_attribute_group');
        $modelGroup->setAttributeGroupName($name)
            ->setAttributeSetId($attributeSetIdHairZone)
            ->setSortOrder($sort);
        $modelGroup->save();
    }

    // Add attributes to attr-groups

    $attributes = array(
        'General' => array(
            array(
                'attr_code' => 'name',
                'sort' => 10
            ),
            array(
                'attr_code' => 'sku',
                'sort' => 20
            ),
            array(
                'attr_code' => 'status',
                'sort' => 30
            ),
            array(
                'attr_code' => 'tax_class_id',
                'sort' => 40
            ),
            array(
                'attr_code' => 'url_key',
                'sort' => 50
            ),
            array(
                'attr_code' => 'visibility',
                'sort' => 60
            ),
            array(
                'attr_code' => 'manufacturer',
                'sort' => 70
            ),
            array(
                'attr_code' => 'news_from_date',
                'sort' => 80
            ),
            array(
                'attr_code' => 'news_to_date',
                'sort' => 90
            ),
            array(
                'attr_code' => 'country_of_manufacture',
                'sort' => 100
            )
        ),
        'Prices' => array(
            array(
                'attr_code' => 'price',
                'sort' => 10
            ),
            array(
                'attr_code' => 'group_price',
                'sort' => 20
            ),
            array(
                'attr_code' => 'cost',
                'sort' => 30
            ),
            array(
                'attr_code' => 'tier_price',
                'sort' => 40
            ),
            array(
                'attr_code' => 'special_price',
                'sort' => 50
            ),
            array(
                'attr_code' => 'special_from_date',
                'sort' => 60
            ),
            array(
                'attr_code' => 'special_to_date',
                'sort' => 70
            ),
            array(
                'attr_code' => 'price_view',
                'sort' => 80
            ),
            array(
                'attr_code' => 'msrp_enabled',
                'sort' => 90
            ),
            array(
                'attr_code' => 'msrp_display_actual_price_type',
                'sort' => 100
            ),
            array(
                'attr_code' => 'msrp',
                'sort' => 110
            )
        ),
        'BDH Attributes' => array(
            array(
                'attr_code' => 'color',
                'sort' => 10
            ),
            array(
                'attr_code' => 'bdh_length',
                'sort' => 20
            ),
            array(
                'attr_code' => 'bdh_weight',
                'sort' => 30
            )
        ),
        'Meta Information' => array(
            array(
                'attr_code' => 'meta_title',
                'sort' => 10
            ),
            array(
                'attr_code' => 'meta_keyword',
                'sort' => 20
            ),
            array(
                'attr_code' => 'meta_description',
                'sort' => 30
            )
        ),
        'Images' => array(
            array(
                'attr_code' => 'thumbnail',
                'sort' => 10
            ),
            array(
                'attr_code' => 'small_image',
                'sort' => 20
            ),
            array(
                'attr_code' => 'gallery',
                'sort' => 30
            ),
            array(
                'attr_code' => 'image',
                'sort' => 40
            ),
            array(
                'attr_code' => 'media_gallery',
                'sort' => 50
            )
        ),
        'Description' => array(
            array(
                'attr_code' => 'description',
                'sort' => 10
            ),
            array(
                'attr_code' => 'short_description',
                'sort' => 20
            )
        ),
        'Design' => array(
            array(
                'attr_code' => 'custom_design',
                'sort' => 10
            ),
            array(
                'attr_code' => 'custom_design_from',
                'sort' => 20
            ),
            array(
                'attr_code' => 'custom_design_to',
                'sort' => 30
            ),
            array(
                'attr_code' => 'custom_layout_update',
                'sort' => 40
            ),
            array(
                'attr_code' => 'options_container',
                'sort' => 50
            ),
            array(
                'attr_code' => 'page_layout',
                'sort' => 60
            )
        ),
        'Recurring Profile' => array(
            array(
                'attr_code' => 'is_recurring',
                'sort' => 10
            ),
            array(
                'attr_code' => 'recurring_profile',
                'sort' => 20
            )
        ),
        'Gift Options' => array(
            array(
                'attr_code' => 'gift_message_available',
                'sort' => 10
            )
        )
    );

    foreach ($attributes as $groupName => $attrArr) {
        $attributeGroupIdBase = $installer->getAttributeGroupId('catalog_product', $attributeSetIdBase, $groupName);
        $attributeGroupIdHairZone = $installer->getAttributeGroupId('catalog_product', $attributeSetIdHairZone, $groupName);
        foreach ($attrArr as $attr) {
            try {
                $installer->addAttributeToGroup('catalog_product', $attributeSetIdBase, $attributeGroupIdBase, $attr['attr_code'], $attr['sort']);
                if($attr['attr_code'] != 'bdh_weight'){
                    $installer->addAttributeToGroup('catalog_product', $attributeSetIdHairZone, $attributeGroupIdHairZone, $attr['attr_code'], $attr['sort']);
                }
            } catch (Excpetion $e) {
                Mage::logException($e);
                Mage::log('Error in adding attributes to Group  ' . $groupName . ' ERROR: ' . $e->getMessage());
            }
        }
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}