<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$_content = <<<EOD
 <div class="img_directory">
    <img src="{{skin  url='images/slider/faq_banner.gif'}}" alt="National Directory">
</div>
<div class="directory-info">
    <h1>National Directory</h1>
    <p>Get listed in our national directory! Send your information to info@belladreamhair.com</p>
    <ul>
        <li>
            <h2>Stylists</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>ERICKA CHRRIE</h3>
                        <p>Sylvia's Hair Salon</p>
                        <p>301 Mcmechen Street</p>
                        <p> Baltimore MD 21217</p>
                        <p>410.728.0002</p>
                    </li>
                    <li>
                        <h3>BQUTE BEAUTYBAR & BOUTIQUE</h3>
                        <p>2140 East Palmdale Boulevard</p>
                        <p>Suite P</p>
                        <p>Palmdale, CA 93550</p>
                        <p>877-71bQute (877-712-7883)</p>
                        <p><a href=http://www.bqute.com">www.bqute.com</a></p>
                        <p><a href="mailto:bqute2@gmail.com">bqute2@gmail.com</a> </p>
                    </li>
                    <li>
                        <h3>Myisha Pettigrew</h3>
                        <p>Meek Salon, Inc</p>
                        <p>1549 W. Passyunk Ave.</p>
                        <p> Phildelphia PA 19145</p>
                        <p>215.339.1950</p>
                    </li>
                </ul>
            </div>
            <div class="directory-col2">
                <ul>
                    <li>
                        <h3>STYLES BY STAR</h3>
                        <p>10100 Beechnut</p>
                        <p>Houston TX</p>
                        <p>832.332.5840</p>
                    </li>
                    <li>
                        <h3>ERICA COX</h3>
                        <p><a href="mailto:EricaTheGlamatician@gmail.com"> EricaTheGlamatician@gmail.com</a> </p>
                        <p>901.292.6623</p>
                    </li>
                    <li>
                        <h3> HAIRSTRING</h3>
                        <p>Stylist: Corinne</p>
                        <p>15125 ventura Blvd #25</p>
                        <p> Sherman oaks,CA</p>
                        <p>800-385-4556</p>
                        <p>818-207-4776</p>
                        <p><a href=http://www.hairstringllc.com">www.hairstringllc.com</a> </p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <h2>Makeup Artists</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>Starlynn Burden</h3>
                        <p><a href=http://www.thebombshellbeauty.com">www.thebombshellbeauty.com</a></p>
                        <p><a href="starlynn@thebombshellbeauty.com">starlynn@thebombshellbeauty.com</a></p>
                    </li>

                </ul>
            </div>
            <div class="directory-col2">
                <ul>
                    <li>
                        <h3>Makeup By ShaLeena</h3>
                        <p><a href=http://www.makeupbyshaleena.webs.com">www.makeupbyshaleena.webs.com</a></p>
                        <p><a href="makeupbyshaleena@yahoo.com">makeupbyshaleena@yahoo.com.</a></p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <h2>Photographers</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>AISHAH IMANI</h3>
                        <p>310.483.4471</p>
                        <p><a href="mailto:artisticinceptions@gmail.com">artisticinceptions@gmail.com</a> </p>
                        <p><a href=http://www.artisticinceptions.tumbler.com">www.artisticinceptions.tumbler.com</a></p>
                        <p>connect with me on facebook</p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <h2>Nail Technicians</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>Jasmine Clark JASMINE CLARK</h3>
                        <p>Chicago, IL</p>
                        <p>513.461.1604</p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <h2>Estheticians</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>Jasmine Clark JASMINE CLARK </h3>
                        <p> Chicago, IL</p>
                        <p>513.461.160</p>
                    </li>

                </ul>
            </div>
            <div class="directory-col2">
                <ul>
                    <li>
                        <h3>DAWN SIMS</h3>
                        <p>SpaLashme Beauty Bar</p>
                        <p><a href=http://www.spalashme.com">www.spalashme.com</a></p>
                        <p>Goodlettville, TN</p>
                        <p>615.775.8383</p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <h2>Fashion</h2>
            <div class="directory-col1">
                <ul>
                    <li>
                        <h3>Sharmooz</h3>
                        <p><a href=http://www.sharmooz.com"> www.sharmooz.com</a></p>
                        <p>From infants to adults, Sharmooz provides a </p>
                        <p>proven method of hair protection and preventing</p>
                        <p> dry facial skin. Are you sleeping on a</p>
                        <p>Sharmooz?</p>
                        <p>800.405.6416</p>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>
EOD;
$pagedata = Array (
    'title' => "Vendor directory",
    'root_template' => 'one_column',
    'identifier' => 'vendor-directory',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$directory = Mage::getModel('cms/page')->load('vendor-directory');
if (!$directory->getId()) {
    $directory->setData($pagedata)->save();
}else{
    $directory->setContent($_content)->save();
}



$installer->endSetup();