<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/3/14
 * Time: 3:55 PM
 */

try {
    $installer = $this;
    $installer->startSetup();

    $websiteId = Mage::app()->getWebsite()->getId();
    $storeId = Mage::app()->getStore();

    // Create attribute with type 'customer'
    $installer->addAttribute('customer', 'old_id', array(
        'type' => 'text',
        'input' => 'text',
        'label' => 'Old Id',
        'visible' => false,
        'required' => false,
        'sort_order' => 15,
        'is_visible' => 0,
        'is_user_defined' => 1
    ));

    $installer->addAttribute('customer', 'set_password', array(
        'type' => 'text',
        'input' => 'text',
        'label' => 'Set Password Again',
        'visible' => false,
        'required' => false,
        'sort_order' => 16,
        'is_visible' => 0,
        'is_user_defined' => 1
    ));

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}