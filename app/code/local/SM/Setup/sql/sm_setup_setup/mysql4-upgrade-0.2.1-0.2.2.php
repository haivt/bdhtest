<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/3/14
 * Time: 3:55 PM
 */
try {
    $installer = $this;
    $installer->startSetup();

    $content = <<<EOD
   <div class="mobile-nav-container">
<div id="menu-active" class="show-click">
    <span>Menu</span>
</div>
<ul id="nav-mobile">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='igallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
<li><a href="{{store url='shop.html'}}">SHOP</a></li>
</ul>
</div>
EOD;
    $staticBlock = array(
        'title' => 'Mobile Top Menu',
        'identifier' => 'mobile_top_menu',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('mobile_top_menu');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($content)->save();
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}