<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:28 AM
 */

try {
    $installer = $this;
    $installer->startSetup();

    // Create about-header block
    $content_header = <<<EOD
    <div class="img-gallery-header">
        <img class="img-header" src="{{skin url='my_igallery/images/gallery-header-mobile.jpg'}}" alt="" />
    </div>
EOD;

    $gallery_headerBlock = array(
        'title' => 'Gallery Header Block',
        'identifier' => 'gallery_header_block',
        'content' => $content_header,
        'is_active' => 1,
        'stores' => array(0)
    );

    $ahBlock = Mage::getModel('cms/block')->load('gallery_header_block');
    if (!$ahBlock->getId()) {
        Mage::getModel('cms/block')->setData($gallery_headerBlock)->save();
    } else {
        $ahBlock->setContent($content_header)->save();
    }

    $content = <<<EOD
   <div class="nav-container">
<ul id="nav">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='igallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
<li><a href="{{store url='shop'}}">SHOP</a></li>
</ul>
</div>
EOD;
    $staticBlock = array(
        'title' => 'Menu',
        'identifier' => 'top_menu',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('top_menu');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($content)->save();
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}