<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$_content = <<<EOD
 <div class="img_faq">
    <img src="{{skin  url='images/slider/faq_banner.gif'}}">
</div>
<div class="faq_info">
    <h1>FAQ'S</h1>
    <p class="faq_title">
        Here at Bella Dream Hair we pride ourselves on providing high quality 100% virgin human hair along with the supreme client care experience. We make every effort to match each order to your exact specifications.
        All orders are thoroughly inspected for accuracy. Because the hair is 100% virgin human hair most pieces can come from separate donors so patterns, textures, and colors may vary.
        It’s best not to separate orders and order at the same time, so we can make every effort possible to match your order.
    </p>
    <h3>Business Hours</h3>
    <p>Monday through Friday from 8:00 AM to 5:00 PM PST, excluding weekends and holidays</p>
    <h3>How much hair do I need to order?</h3>
    <p>
        Our hair is not sold in packs, it's measured by the ounce. 
        We recommend a minimum of 8 oz. for 10-16 inches and 12 oz.
        (or more) for 18-24 inches.  Remember the rule of thumb, the longer the hair the more bundles you may need. 
        Many of our client photos shown online are wearing more than the suggested amount for added fullness.
    </p>
    <h3>Processing Time and Shipping</h3>
    <p>
        Regular orders are shipped within 2-4 business days (excluding holidays and weekends), this includes the time needed to prepare the order to be shipped. Custom orders like wigs and color can have a 7 business day processing time.
        All standard orders are shipped using UPS 3 Day Select Shipping (Saturday delivery if available in your area).

        • Custom lace wigs have a 10-14 business day processing time.
        • Standard wigs have a 7-10 business day processing time.
        • Tracking numbers are updated with in 24hours of the orders shipment.
        • At this time we are not offering Express Services
    </p>
    <h3>Deliveries/ Shipments</h3>
    <p>
        Your order will be shipped to the address on your order. It’s imperative that you check your order for accuracy prior to finalizing and submitting payment. No refunds will be issued for refused or abandoned shipments. Please note; we are not responsible for carrier delays due to holidays or weather.
    </p>
    <h3>Refunds</h3>
    <p>
        At Bella Dream Hair we pride ourselves and maintain a high level of quality and accuracy. Due to the nature of the products, all sales are final and no refunds are given. Federal law prohibits the return of hair that has been installed.
    </p>
    <h3>Exchanges </h3>
    <p>
        It is vital that you completely inspect your order with 7 (seven) business days from the date it is delivered prior to installation or any alterations for all exchanges. Exchanges can be done within 7 (seven) business days from the date the product is delivered. Custom colored hair, wigs, clip-ins, and special made orders that is not bundled hair cannot be exchange (all sales final).
        If you would like to exchange for a different texture, color, or length it will be at the cost of the customer, plus a 10% Change Order Fee. We are not responsible for shipping cost, unless we deem the product defective and we will not ship new hair until the order is received and inspected.
    </p>
    <h3>Do you offer a discount?</h3>
    <p>
        Yes, currently we have discounts available for licensed hair stylists, students, and servicemen and women in the military.</br>  
        We will need a clear image of your current license and ID and state issued ID emailed to info@belladreamhair.com for discount code to be sent to you. All discount codes should be used at checkout prior to ordering.
    </p>
    <h3>Where does the hair come from?</h3>
    <p>
        Directly from Brazil and other exotic countries. We will specify which country hair comes from for each type of hair for sale.
    </p>
    <h3>How long does the hair last?</h3>
    <p>This is 100% human virgin hair. It should last for up to a year depending on how well you take care of it.</p>
    <h3>How do I take care of the hair?</h3>
    <p>Wash, condition and gently comb the hair.</p>
    <h3>Do you process the hair before you sell it?</h3>
    <p>No, we do not process the hair.</p>
    <h3>How many ounces of hair do I need for my head?</h3>
    <p>On average, 8 ounces of hair will be enough for one head. In some cases, for greater coverage, you might buy as much as 12 ounces. Consult with your hair stylist to find out what your specific needs are.</p>
    <h3>How much does it cost to ship the hair?</h3>
    <p>Shipping is $25.00 via UPS 3 Days Select Shipping depending on your location.</p>
    <h3>Do you sell wholesale?</h3>
    <p>Yes. Email us at info@belladreamhair.com for more details about our wholesale program.</p>
    <h3>What about layaway?</h3>
    <p>We pride ourselves on having the best prices in the business. We sell top-quality hair at a fraction of the price of others. However, if you still need help making that purchase, you may put down a 50% non-refundable deposit on hair and pay the rest within 30 days. There is a 10% change order fee if you choose to switch your order to another type of hair.</p>
    <h3>I am White/Asian/Latina/etc. Can I buy hair extensions or is this just for Black women?</h3>
    <p>We are unique in that we are one of the few Black-owned hair extensions businesses and have been using hair extensions for 20 years. However, we serve all races and recognize that other races have gradually been using hair extensions at an increasing rate. If you are unsure of how to use the extensions, contact us and we will put you in contact with a stylist who will help you.</p>
    <h3>Photographs and Client Photo</h3>
    <p>Photographs we post on social network sites such as Twitter and Instagram are used to showcase customer's orders and hairstyles using Bella Dream Hair. Different variables such as lighting, use of flash, computer monitor settings, or even customers modifying their own hair once received can give different appearances.</p>
</div>
EOD;
$pagedata = Array (
    'title' => "FAQ",
    'root_template' => 'one_column',
    'identifier' => 'frequently-asked-questions',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('frequently-asked-questions');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
}else{
    $homePage->setContent($_content)->save();
}

$_content = <<<EOD
 <div class="customer_review_img"><img src="{{skin url='images/slider/faq_banner.gif'}} " alt="Customer Reviews" /></div>
<div class="Testimonial_info">
	<div class="container">
		<h1>Testimonials</h1>
		<ul>
			<li>
				<div class="customer-img item-left"><img src="{{skin url='images/icons/tes01.gif'}}" alt="Michelle Anderson" /></div>
				<div class="customer-info item-right">
					<h2>Michelle Anderson</h2>
					<p>"Just wanted to say hello and to send a couple of pictures of me wearing my beautiful Bella Dream Hair extensions. Love, love, love this hair. I'm referring your company to all of my extension wearing friends :) !"</p>
				</div>
			</li>
			<li>
				<div class="customer-img item-left"><img src="{{skin url='images/icons/tes02.gif'}}" alt="Rochelle Boreland" /></div>
				<div class="customer-info item-right">
					<h2>Rochelle Boreland</h2>
					<p>"I've ordered all types of hair. From beauty supply brand "remy" to over priced virgin hair. A lot of vendors make promises about what the hair will do and how it cannot be compared to any other brand. I think it is safe to say that Bella Dream Hair is THE hair that cannot be compared. For the first time I can honestly say I got exactly what I paid for and my extensions look and behave just as promised. The Portugal Deep Wave matched well with the texture of my hair and was not too fine are overly shiny. I've recommended the hair to friends and they are also very pleased!"</p>
				</div>
			</li>
			<li>
				<div class="customer-img item-left"><img src="{{skin url='images/icons/tes03.gif'}}" alt="Kiarra" /></div>
				<div class="customer-info item-right">
					<h2>Kiarra</h2>
					<p>"This is by far the BEST hair I ever had. I wanted to order something that looks the most natural like my own hair so I ordered the 16 inch Brazilian Straight Hair. I have never received so many compliments and when I told people that it was weave, they could not believe it and that was the best feeling ever. =) Bella Dream Hair is now the ONLY hair I will ever put in my hair. I HIGHLY recommend it....you will not regret it!"</p>
				</div>
			</li>
		</ul>
	</div>
</div>
EOD;
$pagedata = Array (
    'title' => "Customer reviews",
    'root_template' => 'one_column',
    'identifier' => 'testimonials',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('testimonials');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
}else{
    $homePage->setContent($_content)->save();
}

$installer->endSetup();