<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/3/14
 * Time: 3:55 PM
 */
try {
    $installer = $this;
    $installer->startSetup();

    $storeId = Mage::app()->getStore()->getStoreId();
    $store = Mage::getModel('core/store')->load($storeId);
    $group = Mage::getModel('core/store_group')->load($store->getGroupId());

    $hairzExist = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToFilter('name', 'Hair Zone')->getFirstItem();
    if($hairzExist->getId() == null) {
        $cat = Mage::getModel('catalog/category');
        $cat->setPath('1/' . $group->getRootCategoryId())
            ->setName("Hair Zone")
            ->setUrlKey("hair_zone")
            ->setStoreId($storeId)
            ->setIsActive(1)
            ->setIncludeInMenu(0)
            ->setIsAnchor(1)
            ->save();
    }

    $hairzCat = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToFilter('name', 'Hair Zone')->getFirstItem();
    $categories = array(
        0 => array(
            'url_key' => 'frontals',
            'name'=> 'Frontals'
        ),
        1 => array(
            'url_key' => 'closures',
            'name'=> 'Closures'
        ),

    );

    foreach ($categories as $sortOrder => $data) {
        $exist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('name', $data['name'])->getFirstItem();
        if ($exist->getId() == null) {
            $category = Mage::getModel('catalog/category')
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIncludeInMenu(1)
                ->setIsAnchor(1)
                ->setPath('1/' . $group->getRootCategoryId(). '/' . $hairzCat->getId())
                ->save();
        }
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}