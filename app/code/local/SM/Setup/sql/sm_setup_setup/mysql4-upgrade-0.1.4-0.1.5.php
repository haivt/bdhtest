<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$_content = <<<EOD
 <div class="img_contact"><img src="{{skin  url='images/slider/faq_banner.gif'}}" alt="Contact Us" /></div>
<p>{{block type="core/template" name="contactForm" form_action="/contacts/index/post" template="contacts/form.phtml"}}</p>
EOD;
$pagedata = Array (
    'title' => "Contact Us",
    'root_template' => 'one_column',
    'identifier' => 'contact',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$directory = Mage::getModel('cms/page')->load('contact');
if (!$directory->getId()) {
    $directory->setData($pagedata)->save();
}else{
    $directory->setContent($_content)->save();
}



$installer->endSetup();