<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:04 AM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


//------------------------ creat home page----------------------------
$_content = <<<EOD
    <p>{{block type="page/html" name="home.slide.show" alias="slideshow" template="page/html/slideshow.phtml" block_name="NEW PRODUCTS"}}</p>
<div class="home_detail">
	<div class="dream_hair_info item-left">
		<p class="bella_title">Bella Dream Hair</p>
		<p class="natural">Natural Human Hair Weaves.</p>
		<span>We believe in offering the highest level of hair and service at <br /> affordable prices. All of our hair is made from 100% human hair, <br /> comes in natural colors, and can be dyed to achieve any specific <br /> hue. Our beautiful hair extensions are crafted from the highest quality<br /> human hair and are meticulously put together following natural hair <br /> growth patterns, resulting in the silkiest, smoothest hair extensions. </span>
		<div class="wrap-link"><a href="#"><span>LEARN MORE</span></a></div>
	</div>
	<div class="item-right">
		{{block type="page/html" name="home.slide.show" alias="slideshow" template="page/html/homebestseller.phtml" block_name="NEW PRODUCTS"}} {{block type="newsletter/subscribe" name="home.newsletter" template="newsletter/subscribe.phtml"}}
	</div>
	<div class="clear">&nbsp;</div>
</div>
<div class="as_soon_on">
	<h3 class="title"><span>AS SOON ON</span></h3>
	<ul>
		<li><a><img src="{{skin url='images/icons/brand01.png'}}" alt="Rochelle Ranique" /></a></li>
		<li><a href="http://www.upscalemagazine.com/"><img src="{{skin url='images/icons/brand02.png'}}" alt="Upscale" /></a></li>
		<li><a href="http://www.vibe.com/article/v-charity-night-bella-dream-hair-autism-speaks"><img src="{{skin url='images/icons/brand03.png'}}" alt="Vibe" /></a></li>
		<li><a href="http://www.vibevixen.com/2011/08/celebrity-endorsed-wig-lines-vs-owners-which-would-you-buy/"><img src="{{skin url='images/icons/brand04.png'}}" alt="Vixen" /></a></li>
		<li><a href="http://www.blackenterprise.com/magazine/"><img src="{{skin url='images/icons/brand05.png'}}" alt="Black enterprise" /></a></li>
	</ul>
	<div class="clear">&nbsp;</div>
</div>
EOD;
$pagedata = Array (
    'title' => 'Home page',
    'root_template' => 'one_column',
    'identifier' => 'home_page',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('home_page');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
}

$Config = Mage::app()->getConfig();
$Config ->saveConfig('web/default/cms_home_page', "home_page");
$Config ->saveConfig('design/theme/locale', "dreamhair");
$Config ->saveConfig('design/theme/template', "dreamhair");
$Config ->saveConfig('design/theme/skin', "dreamhair");
$Config ->saveConfig('design/theme/layout', "dreamhair");
$Config ->saveConfig('design/header/logo_src', "images/icons/logo.png");
$Config ->saveConfig('design/footer/copyright', "&copy; 2013 Bella Dream Hair, all rights reserved.");

// --------------------------creat static block------------------------------
$content = <<<EOD
    <ul>
	<li><a href="{{store direct_url="about-us"}}">ABOUT US</a><span class="separate">|</span></li>
	<li><a href="{{store direct_url="privacy-policy"}}">PRIVACY POLICY</a><span class="separate">|</span></li>
	<li class="last privacy"><a href="{{store direct_url="teams-of-use"}}">TERMS OF USE</a></li>
</ul>
EOD;
$staticBlock = array(
    'title' => 'Footer Links',
    'identifier' => 'footer_links',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('footer_links');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
    <div class="footer-addthis">
    <ul class="footer_social">
    <li><a class="social_instagram" title="Follow on Instagram" href="#" target="_blank"><img src="{{skin url='images/icons/social01.png'}}" alt="" /></a></li>
    <li><a class="social_facebook" title="Follow on Facebook" href="#" target="_blank"><img src="{{skin url='images/icons/social02.png'}}" alt="" /></a></li>
    <li><a class="social_twitter" title="Follow on Twitter" href="#" target="_blank"><img src="{{skin url='images/icons/social03.png'}}" alt="" /></a></li>
    </ul>
    </div>
EOD;
$staticBlock = array(
    'title' => 'Social',
    'identifier' => 'social_footer',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('social_footer');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
   <div class="footer-addthis">
    <ul class="footer_social">
    <li><a class="social_instagram" title="Follow on Instagram" href="#" target="_blank"><img src="{{skin url='images/icons/head_sc01.png'}}" alt="" /></a></li>
    <li><a class="social_facebook" title="Follow on Facebook" href="#" target="_blank"><img src="{{skin url='images/icons/head_sc02.png'}}" alt="" /></a></li>
    <li><a class="social_twitter" title="Follow on Twitter" href="#" target="_blank"><img src="{{skin url='images/icons/head_sc03.png'}}" alt="" /></a></li>
    </ul>
    </div>
EOD;
$staticBlock = array(
    'title' => 'Social header',
    'identifier' => 'social_header',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('social_header');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
   <div class="nav-container">
<ul id="nav">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='gallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
<li><a href="{{store url='shop.html'}}">SHOP</a></li>
</ul>
</div>
EOD;
$staticBlock = array(
    'title' => 'Menu',
    'identifier' => 'top_menu',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('top_menu');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}




$installer->endSetup();