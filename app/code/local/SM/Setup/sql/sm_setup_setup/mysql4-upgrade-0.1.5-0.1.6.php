<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:28 AM
 */

try {
    $installer = $this;
    $installer->startSetup();

    //-------------------------creat atribute shop page best seller------------------------------
    $model = Mage::getModel('eav/entity_setup','core_setup');
    $data = array(
        'type' => 'int',
        'group' => 'General',
        'input' => 'boolean',
        'label' => 'Shop Page Best seller',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'is_required' => '0',
        'is_comparable' => '0',
        'is_searchable' => '0',
        'is_unique' => '0',
        'is_configurable' => '0',
        'use_defined' => '0'

    );
    $attribute_code = "shop_page_best_seller";
    $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
    $attribute_id = $attribute->getData('attribute_id');
    if($attribute_id == null) {
        $model->addAttribute('catalog_product', $attribute_code, $data);
    }

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}