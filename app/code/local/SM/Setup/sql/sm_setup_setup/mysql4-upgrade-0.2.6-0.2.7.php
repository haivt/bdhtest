<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$_content = <<<EOD
 <div class="img_faq"><img src="{{skin  url='images/slider/interior-header.jpg'}}" alt="" /></div>
<div class="faq_info faq-page-live">
	<div class="container clearfix">
		<div class="faq-content">
			<h1>FAQ'S</h1>
			<p class="faq_title">At Bella Dream Hair we pride ourselves on providing high quality 100% virgin human hair and a superior client care experience by matching each order to your exact specifications. All orders are thoroughly inspected for quality and accuracy. As the hair is 100% virgin human hair, our products may come from different sources, so patterns, textures, and colors may vary. In order to achieve the most consisitent look, we recommend that you order enough hair to satisfy your needs versus mixing orders.</p>
			<div class="item">
				<h2>Business Hours</h2>
				<p>Monday through Friday from 8:00 AM to 5:00 PM PST, excluding weekends and holidays</p>
			</div>
			<div class="item">
				<h2>How much hair do I need to order?</h2>
				<p>Our hair is not sold in packs, it is measured by the ounce. We recommend a minimum of 8 ounces for lengths between 10-16 inches, and 12 ounces for lengths between 18-24 inches. For lengths greater than 24 inches, we suggest 16 ounces. Â In order to achieve fullness, typlically more bundles are required for longer lengths.</p>
			</div>
			<div class="item">
				<h2>Processing Time and Shipping</h2>
				<p>Regular orders are shipped within 2-3 business days (excluding holidays and weekends). Â This includes time needed to prepare and process the order to be shipped. Custom orders such as wigs and special colors typically take longer to process as follows:</p>
				<ul>
					<li>Custom lace wigs: 10-14 business days</li>
					<li>Standard wigs: Â 7 business days</li>
				</ul>
				<p>All standard orders are shipped using UPS 3 Day Select shipping; tracking numbers are automatically emailed. Saturday delivery is offered depending on availability in your area.</p>
				<p>Please email us at info@belladreamhair.com for express and 1 and 2 day shipping options. Please note that payment and fees must be completed by 12:00 PM PST for these options.</p>
			</div>
			<div class="item">
				<h2>Delivery Information</h2>
				<p>Your order will be shipped to the address on your order. Itâ??s important that you check your order for accuracy prior to finalizing and submitting payment. No refunds will be issued for refused or abandoned shipments. Please note: we are not responsible for carrier delays due to holidays or weather.</p>
			</div>
			<div class="item">
				<h2>Refunds</h2>
				<p>At Bella Dream Hair we pride ourselves on maintaining a high level of quality and accuracy. However, if you are not satisfied with the product, the hair may be returned or exchanged within 5 businesses days. Â The hair must be in its original condition (no installation, cutting or other altering of the hair) and must be in its orignal packaging. Â Exchanges and refunds will only be processed after we have received the hair and have inspected it. Custom colored hair, wigs, clip-ins, and special orders cannot be exchanged (all sales final).Â Federal law prohibits the return of hair that has been installed.  </p>
			</div>
			<div class="item">
				<h2>Exchanges</h2>
				<p>Exchanges may be done within 5 (five) business days from the date the product is delivered. Custom colored hair, wigs, clip-ins, and special orders cannot be exchanged (all sales final).</p>
				<p>Exchanges may be done within 5 (five) business days from the date the product is delivered. Custom colored hair, wigs, clip-ins, and special orders cannot be exchanged (all sales final).</p>
				<p>If you would like to exchange your order for a different texture, color, or length, you will be responsible for the differential in cost plus a 10% Change Order Fee. You will also be responsible for all shipping costs unless we determine that the product is defective. We will not ship replacement hair until the original order is received and inspected.</p>
			</div>
			<div class="item">
				<h2>Where is the hair sourced?</h2>
				<p>We procure our hair from a variety of different sources around the world.</p>
			</div>
			<div class="item">
				<h2>How long does the hair last?</h2>
				<p>This is 100% human virgin hair and should last a long time with proper care.</p>
			</div>
			<div class="item">
				<h2>Do you sell wholesale?</h2>
				<p>We sell to the licensed trade and bulk purchasers. Â Email us at info@belladreamhair.com for more details about our wholesale program.</p>
			</div>
			<div class="item">
				<h2>Bella Dream Hair is for Everyone</h2>
				<p>Bella Dream Hair offers products for every ethnicity. Â Our premium hair looks great whether you are black, white, latina, asian, etc... Â We have many clients from a wide variety of ethnicities who proudly wear our hair.</p>
			</div>
			<div class="item">
				<h2>Photographs and Client Photos</h2>
				<p>Photographs we post on social network sites such as Twitter and Instagram are used to showcase customer orders and hairstyles using Bella Dream Hair. Different variables such as lighting, use of flash, computer monitor settings, or even customers modifying their own hair once received can give different appearances.</p>
			</div>
		</div>
		<div class="shop_now">
			<a href="/shop.html">Shop Now</a>
		</div>
	</div>
</div>
EOD;
$pagedata = Array (
    'title' => "FAQ",
    'root_template' => 'one_column',
    'identifier' => 'frequently-asked-questions',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('frequently-asked-questions');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
}else{
    $homePage->setContent($_content)->save();
}

$content_about = <<<EOD
<div class="about-page-live">
	<div class="about-us">
		<div class="container clearfix">
			<h3 class="title-page"><span>ABOUT US</span></h3>
			<p class="intro-about">Bella Dream Hair – Premium Hair, Wide Selection, Superior Customer Experience</p>
		</div>
	</div>
	<div class="about-content container clearfix">
		<div class="item-left">
			<div class="bx-content">
				<h1>Bella Dream Hair</h1>
				<h2>Premium Quality Human Hair Extension</h2>
				<p>Our clients demand the highest quality hair products and superior customer service. Bella Dream Hair offers premium hair extensions for customers who demand only the best. Our finest hair comes in natural colors or can be custom ordered in a wide variety of hues. We select only the best hair for our weaves and extensions and meticulously reproduce natural growth patterns, resulting in great looking hair in a variety of lengths, textures and colors. Bella Dream Hair is the industry standard for quality and service.<br /><br />
					Bella Dream Hair sources its hair through a network of suppliers dedicated to finding the best quality hair from around the world. We are very selective about our hair products and will reject hair that does not meet our rigorous standards. We take pride in our product offering with careful screening, salon level treatment and also meticulous detail in crafting natual hair growth patterns so that our hair looks great on your head.<br /><br />
					We offer a variety of styles including Brazilian Natual Wave, Straight, Curly and Wavy. Each of these styles comes in a range of lengths and colors to allow you to match your hair style to the right occasion, whether it is for a professional look or something more exotic for special events. With proper care, our hair will maintain its natural look and texture. We offer a variety of installation options for our hair including skin weft extensions, clip-on extensions, micro links and custom wigs. Bella Dream Hair is committed to providing our customers the best solution at competitive prices.<br /><br />
					At Bella Dream Hair, we are focused on delivering the highest quality customer experience. Our easy on-line ordering ensures that your purchase is secure and accurate. We have dedicated quality control experts processing your orders and knowledgeable customer service agents to address your questions. You no longer have to worry about the experience of ordering hair.<br /><br />
					Bella Dream Hair is your one-stop provider of the highest quality hair products.
				</p>
			</div>
		</div>
		<div class="item-right">
			<div class="bx-list">
				<h3>More Information</h3>
				<ul>
					<li><a href="{{store url='testimonials'}}"><span class="button_roll_over">Customer Reviews</span></a></li>
					<li><a href="{{store url='frequently-asked-questions'}}"><span class="button_roll_over">Frequently Asked Questions</span></a></li>
				</ul>
			</div>
			<div class="bx-list bx-list2">
				<ul>
					<li><a href="{{store url='shop.html'}}"><span class="button_roll_over">Shop Now</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
EOD;

$content_headerBlock = array(
    'title' => 'About Content Block',
    'identifier' => 'content_header_block',
    'content' => $content_about,
    'is_active' => 1,
    'stores' => array(0)
);

$ahBlock = Mage::getModel('cms/block')->load('content_header_block');
if (!$ahBlock->getId()) {
    Mage::getModel('cms/block')->setData($content_headerBlock)->save();
} else {
    $ahBlock->setContent($content_about)->save();
}


$content = <<<EOD
   <div class="nav-container">
<ul id="nav">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='shop.html'}}">SHOP</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='igallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
</ul>
</div>
EOD;
$staticBlock = array(
    'title' => 'Menu',
    'identifier' => 'top_menu',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('top_menu');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$_content = <<<EOD
 <div class="img_contact"><img src="{{skin  url='images/slider/contact-banner.jpg'}}" alt="Contact Us" /></div>
<p>{{block type="core/template" name="contactForm" form_action="/contacts/index/post" template="contacts/form.phtml"}}</p>
EOD;
$pagedata = Array (
    'title' => "Contact Us",
    'root_template' => 'one_column',
    'identifier' => 'contact',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$directory = Mage::getModel('cms/page')->load('contact');
if (!$directory->getId()) {
    $directory->setData($pagedata)->save();
}else{
    $directory->setContent($_content)->save();
}

$content_header = <<<EOD
<div class="img-about-header">
    <img class="img-header" src="{{skin url='images/slider/about-header.jpg'}}" alt="" />
</div>
EOD;

$about_headerBlock = array(
    'title' => 'About Header Block',
    'identifier' => 'about_header_block',
    'content' => $content_header,
    'is_active' => 1,
    'stores' => array(0)
);

$ahBlock = Mage::getModel('cms/block')->load('about_header_block');
if (!$ahBlock->getId()) {
    Mage::getModel('cms/block')->setData($about_headerBlock)->save();
} else {
    $ahBlock->setContent($content_header)->save();
}

$content = <<<EOD
    <div class="footer-addthis">
    <ul class="footer_social">
    <li><a class="social_instagram" title="Follow on Instagram" href="http://instagram.com/belladreamhair" target="_blank"><img src="{{skin url='images/icons/social01.png'}}" alt="" /></a></li>
    <li><a class="social_facebook" title="Follow on Facebook" href="https://www.facebook.com/pages/Bella-Dream-Hair/143772912356861" target="_blank"><img src="{{skin url='images/icons/social02.png'}}" alt="" /></a></li>
    <li><a class="social_twitter" title="Follow on Twitter" href="https://twitter.com/belladreamhair" target="_blank"><img src="{{skin url='images/icons/social03.png'}}" alt="" /></a></li>
    </ul>
    </div>
EOD;
$staticBlock = array(
    'title' => 'Social',
    'identifier' => 'social_footer',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('social_footer');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
   <div class="footer-addthis">
    <ul class="footer_social">
    <li><a class="social_instagram" title="Follow on Instagram" href="http://instagram.com/belladreamhair" target="_blank"><img src="{{skin url='images/icons/head_sc01.png'}}" alt="" /></a></li>
    <li><a class="social_facebook" title="Follow on Facebook" href="https://www.facebook.com/pages/Bella-Dream-Hair/143772912356861" target="_blank"><img src="{{skin url='images/icons/head_sc02.png'}}" alt="" /></a></li>
    <li><a class="social_twitter" title="Follow on Twitter" href="https://twitter.com/belladreamhair" target="_blank"><img src="{{skin url='images/icons/head_sc03.png'}}" alt="" /></a></li>
    </ul>
    </div>
EOD;
$staticBlock = array(
    'title' => 'Social header',
    'identifier' => 'social_header',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('social_header');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$_content = <<<EOD
<div class="customer_review_img"><img src="{{skin url='images/slider/interior-header.jpg'}} " alt="Customer Reviews" /></div>
<div class="customer-page">
<div class="container clearfix">
<h1><span>Customer Reviews</span></h1>
<div class="shop_now"><a href="/shop.html">Shop Now</a></div>
<ul class="list-cus">
<li>
<h2>Michelle Anderson</h2>
<p>"Just wanted to say hello and to send a couple of pictures of me wearing my beautiful Bella Dream Hair extensions. Love, love, love this hair. I'm referring your company to all of my extension wearing friends :-) !"</p>
</li>
<li>
<h2>Rochelle Boreland</h2>
<p>"I've ordered all types of hair. From beauty supply brand "remy" to over priced virgin hair. A lot of vendors make promises about what the hair will do and how it cannot be compared to any other brand. I think it is safe to say that Bella Dream Hair is THE hair that cannot be compared. For the first time I can honestly say I got exactly what I paid for and my extensions look and behave just as promised. The Portugal Deep Wave matched well with the texture of my hair and was not too fine are overly shiny. I've recommended the hair to friends and they are also very pleased!"</p>
</li>
<li>
<h2>Kiarra</h2>
<p>"This is by far the BEST hair I ever had. I wanted to order something that looks the most natural like my own hair so I ordered the 16 inch Brazilian Straight Hair. I have never received so many compliments and when I told people that it was weave, they could not believe it and that was the best feeling ever. =) Bella Dream Hair is now the ONLY hair I will ever put in my hair. I HIGHLY recommend it....you will not regret it!"</p>
</li>
<li>
<h2>Janelle Addy</h2>
<p>"The latest buzz is "Bella Dream Hair", There aren't too many hair extenstions on the market that blend well with African-Americans. I've never purchased hair by the ounce but I HAD to see for myself. Talk about nervous?! I was so excited to receive my order, Brazilian Curly 10 &amp; 12in, and get my hair done! Bella Dream Hair gets an A++ from me. It's soft and manageable and the curls return with ease. Thank you Bella, I am no longer a 2 pack!! :-)"</p>
</li>
<li>
<h2>Lexi</h2>
<p>"My mom bought me the wavy hair 9 months ago and I love it! I can't wait until she buys me some more!!!"</p>
</li>
<li>
<h2>J. Diaz</h2>
<p>"Lately everyone has been complimenting me on my Luxurious Hair. I'm Loving my Bella Dream Hair! I'm Feeling Sexy 24-7. I've been wearing extensions since a Teen and this has been the Best Hair I've ever found! I'm going to continue to tell all My Friends and Family about Bella Dream Hair! Brazilian Wavy is my Favorite. Thank you so much Bella! !"&Acirc;</p>
</li>
<li>
<h2>Geraldine</h2>
<p>"I absolutely love my portugal deep wave hair from bella dream hair. The hair is so soft and easy to manage. It is also very versatile. I intially wore it curly and now i have it flat ironed straight. I am more than satisfied with my hair."</p>
</li>
<li>
<h2>Tina Baker Hines</h2>
<p>"I ordered the Bella Dream Hair brazillian wave and I love how managable it is. There isn't much upkeep needed because the hair is good, quality hair. I can wash the hair and go-I love that. I will definitley be ordering again!"</p>
</li>
<li>
<h2>Deiajane</h2>
<p>"I wear weaves due to my active lifestyle; working,parenting and exercising play big roles in my life and being able to have hair that is not hard to manage is something I always look for. I use to purchase 100% Virgin Indian Hair and thought I was in love with it, that is not until I found BellaDreamHair 100% Virgin Brazilian Hair. This hair has made all the difference. The texture of hair is almost an exact match to mine, and the quality is better than any Indian Hair I've purchased. I have not experienced any shedding, which is very important, you never want your hair to look thin or brittle. I have washed,blow-dried, dyed and styled BellaDreamHair without any issues with texture or volume. This hair has made me a true believer and I will never go back to Indian Hair again. Not to mention, the price and quality of is affordable and amazing. Thank you so much!!! #TeamBellaDreamHair all the way!! :-)</p>
</li>
<li>
<h2>Alicia</h2>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
<li>
<p><span style="font-size: 1.5em;">Alicia</span></p>
</li>
<li>
<ul class="list-cus">
<li>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
</ul>
</li>
<li>
<h2>Leslie</h2>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
<li>
<ul class="list-cus">
<li>
<h2>Alicia</h2>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
<li>
<p><span style="font-size: 1.5em;">Leslie</span></p>
</li>
<li>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
<li>
<ul class="list-cus">
<li>
<h2>Alicia</h2>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
<li>
<h2>Leslie</h2>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
<li>
<p><span style="font-size: 1.5em;">Alicia&nbsp;</span></p>
</li>
<li>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
<li>
<ul class="list-cus">
<li>
<h2>Leslie</h2>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
<li>
<ul class="list-cus">
<li>
<h2>Alicia</h2>
<p>"BellaDreamHair is the BOMB. I have NEVER had a brand of hair that I have purchased more than twice, That says ALOT!! This hair is so soft, KEEPS its shape, Curls BOMB (as u can see), Straightens Perfect. I can't say enough about this hair. Oh and 1 more thing if you wake up in the morning and forget to comb it, its still no tangles!! Now tell me what other hair does that!? Whoop there it is BELLADREAMHAIR is IT!!"</p>
</li>
<li>
<h2>Leslie</h2>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
<li>
<p><span style="font-size: 1.5em;">Leslie</span></p>
</li>
<li>
<ul class="list-cus">
<li>
<p>"I simply cannot live without Bella Dream Hair! I have tried many different extensions over the years, so I consider myself a weave-ologist! Bella Dream Hair is by far, hands down, the best hair in the game. I would never, ever put anything else in my hair except Bella Dream Hair! So if you see me somewhere, you don't even have to ask! It is definitely Bella Dream Hair!!</p>
</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>
</div>
</div>
EOD;
$pagedata = Array (
    'title' => "Customer reviews",
    'root_template' => 'one_column',
    'identifier' => 'testimonials',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('testimonials');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
}else{
    $homePage->setContent($_content)->save();
}

$content = <<<EOD
   <div class="mobile-nav-container">
<div id="menu-active" class="show-click">
    <span>Menu</span>
</div>
<ul id="nav-mobile">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='shop.html'}}">SHOP</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='igallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
</ul>
</div>
EOD;
$staticBlock = array(
    'title' => 'Mobile Top Menu',
    'identifier' => 'mobile_top_menu',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('mobile_top_menu');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$installer->endSetup();