<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$content = <<<EOD
   <div class="nav-container">
<ul id="nav">
<li><a href="{{store}}">HOME</a></li>
<li><a href="{{store url='shop.html'}}">SHOP</a></li>
<li><a href="{{store url='about'}}">ABOUT</a></li>
<li><a href="{{store url='gallery'}}">GALLERY</a></li>
<li><a href="{{store url='contact'}}">CONTACT</a></li>
</ul>
</div>
EOD;
$staticBlock = array(
    'title' => 'Menu',
    'identifier' => 'top_menu',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('top_menu');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}


$_content = <<<EOD
    <p>{{block type="page/html" name="home.slide.show" alias="slideshow" template="page/html/slideshow.phtml" block_name="NEW PRODUCTS"}}</p>
<div class="home_detail">
	<div class="dream_hair_info item-left">
		<p class="bella_title">Bella Dream Hair</p>
		<p class="natural">Natural Human Hair Weaves.</p>
		<span>
		Our clients demand the highest quality hair products and superior <br /> customer service. Bella Dream Hair offers premium hair extensions for <br />customers looking for the best quality. Our finest hair comes in <br />natural colors or can be custom ordered in a wide variety of hues. <br />We select only the best hair for our extensions and meticulously <br />reflect growth patterns, resulting in natural looking hair with a variety <br /> of lengths, textures and colors. Bella Dream Hair is the industry<br /> standard for quality and service.
		</span>
		<div class="wrap-link"><a href="#"><span>LEARN MORE</span></a></div>
	</div>
	<div class="item-right">
		{{block type="page/html" name="home.slide.show" alias="slideshow" template="page/html/homebestseller.phtml" block_name="NEW PRODUCTS"}} {{block type="newsletter/subscribe" name="home.newsletter" template="newsletter/subscribe.phtml"}}
	</div>
	<div class="clear">&nbsp;</div>
</div>
<div class="as_soon_on">
	<h3 class="title"><span>AS SOON ON</span></h3>
	<ul>
		<li><a><img src="{{skin url='images/icons/brand01.png'}}" alt="Rochelle Ranique" /></a></li>
		<li><a href="http://www.upscalemagazine.com/"><img src="{{skin url='images/icons/brand02.png'}}" alt="Upscale" /></a></li>
		<li><a href="http://www.vibe.com/article/v-charity-night-bella-dream-hair-autism-speaks"><img src="{{skin url='images/icons/brand03.png'}}" alt="Vibe" /></a></li>
		<li><a href="http://www.vibevixen.com/2011/08/celebrity-endorsed-wig-lines-vs-owners-which-would-you-buy/"><img src="{{skin url='images/icons/brand04.png'}}" alt="Vixen" /></a></li>
		<li><a href="http://www.blackenterprise.com/magazine/"><img src="{{skin url='images/icons/brand05.png'}}" alt="Black enterprise" /></a></li>
	</ul>
	<div class="clear">&nbsp;</div>
</div>
EOD;
$pagedata = Array (
    'title' => 'Home page',
    'root_template' => 'one_column',
    'identifier' => 'home_page',
    'content' => $_content,
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$homePage = Mage::getModel('cms/page')->load('home_page');
if (!$homePage->getId()) {
    $homePage->setData($pagedata)->save();
} else {
    $homePage->setContent($_content)->save();
}

$installer->endSetup();