<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$content = <<<EOD
<div class="menu_step_by_step">
<div class="container clearfix">
<ul class="shop_menu">
<li class="nav_active step1"><span class="number">1</span><span class="text">Style</span></li>
<li class="step2"><span class="number">2</span><span class="text">Length &amp; Weight</span></li>
<li class="step3"><span class="number">3</span><span class="text">Color</span></li>
</ul>
</div>
</div>
<div id="breadcrumbs">
<p><span class="title">Your Selection:</span> <em id="selection_style"> <span data-step="style"><strong class="step1-product-name"></strong> (<a href="{{store url="shop.html"}}">Edit</a>)</span> </em> <em id="selection_lengthandweight"> <strong>|</strong> <span data-step="lengthandweight">Length: 22" &emsp;Weight: <em>4oz</em> (<a href="javascript:void(0)">Edit</a>)</span> </em> <em id="selection_color"> <strong>|</strong> <span data-step="color">Color: <strong>...</strong> (<a href="javascript:void(0)">Edit</a>)</span> </em></p>
</div>
EOD;
$staticBlock = array(
    'title' => 'Step shop',
    'identifier' => 'step_by_step',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('step_by_step');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$installer->endSetup();