<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/2/14
 * Time: 10:28 AM
 */

try {
    $installer = $this;
    $installer->startSetup();

    $storeId = Mage::app()->getStore()->getStoreId();
    $store = Mage::getModel('core/store')->load($storeId);
    $group = Mage::getModel('core/store_group')->load($store->getGroupId());

    $cat = Mage::getModel('catalog/category');
    $cat->setPath('1/' . $group->getRootCategoryId())
        ->setName("Shop")
        ->setUrlKey("shop")
        ->setStoreId($storeId)
        ->setIsActive(1)
        ->setIncludeInMenu(0)
        ->setIsAnchor(1)
        ->save();

    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}