<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 5/6/14
 * Time: 8:19 PM
 */
class SM_Page_Block_Page_Html_Pager extends Mage_Page_Block_Html_Pager
{
    public function getLastNum()
    {
        $collection = $this->getCollection();
        if($this->getRequest()->getModuleName() == 'sales' && $this->getRequest()->getControllerName() == 'order' && $this->getRequest()->getActionName() == 'history'){
            if($collection->getCurPage() != $this->getLastPageNum()){
                return $collection->getPageSize() * $collection->getCurPage();
            }
            return $collection->count();
        }
        return $this->getCollection()->getLastPageNumber();
    }
}