<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Giga
 * Date: 4/26/14
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
class SM_Rewrite_ManageController extends Mage_Core_Controller_Front_Action {

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('customer_newsletter')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('Newsletter Subscription | Bella Dream Hair'));
        $this->renderLayout();
    }

}