<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Giga
 * Date: 4/26/14
 * Time: 10:45 AM
 * To change this template use File | Settings | File Templates.
 */ 
class SM_Rewrite_Block_CatalogSearch_Result extends Mage_CatalogSearch_Block_Result {
    protected function _prepareLayout()
    {
        // add Home breadcrumb
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadcrumbs) {
            $title = $this->__("Search results for: '%s'", $this->helper('catalogsearch')->getQueryText());

            $breadcrumbs->addCrumb('home', array(
                'label' => $this->__('Home'),
                'title' => $this->__('Go to Home Page'),
                'link'  => Mage::getBaseUrl()
            ))->addCrumb('search', array(
                    'label' => $title,
                    'title' => $title
                ));
        }

        // modify page title
        $title = $this->__("Search Results | Bella Dream Hair");
        $this->getLayout()->getBlock('head')->setTitle($title);

        return Mage_Core_Block_Abstract::_prepareLayout();
    }
}