<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 2:53 PM
 */
class SM_Order_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getOrders($query)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $items = $readConnection->fetchAll($query);
        return $items;
    }

    /**
     * @param $collection Mage_Sales_Model_Resource_Order_Collection
     * @param $orders SM_Order_Model_Resource_Orders_Python_Collection
     * @param $id
     */
    public function mergeOrders($magentoOrders, $orders)
    {
        $magentoOrders->load();
        $data = array();
        $count = count($orders);
        foreach ($orders as $order) {
            $newOrder = Mage::getModel('sales/order');
            $data['is_old'] = true;
            $data['old_id'] = $order->getId();
            $data['ship_name'] = $order->getBillingDetailFirstName() . ' ' . $order->getBillingDetailLastName();
            $data['created_at'] = $order->getTime();
            $data['total'] = $order->getTotal();
            $data['status_label'] = $order->getStatusLabel();
            $newOrder->setData($data);
            $magentoOrders->addItem($newOrder);
        }
        Mage::getSingleton('core/session')->setNumberNewItem($count);
        return $magentoOrders;
    }
}