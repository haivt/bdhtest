<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/3/14
 * Time: 3:55 PM
 */
try {
    $installer = $this;
    $installer->startSetup();

    $_helper = Mage::helper('sm_setup');
    $cfg = Mage::app()->getConfig();
    $_emailCfg = array(
        array('sales_email/order/pending_template','Pending Order'),
        array('sales_email/order/pending_template_guest','Pending Order for Guest'),
    );
    foreach($_emailCfg as $_email){
        $_emailTempId = $_helper->_getEmailId($_email['1']);
        if($_emailTempId){
            $cfg->saveConfig($_email['0'],$_emailTempId);
        }
    }
    $installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}