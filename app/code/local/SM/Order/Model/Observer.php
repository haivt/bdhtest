<?php

class SM_Order_Model_Observer extends Varien_Event_Observer {

    public function sendCompletedOrderMail($observer) {

        $order = $observer->getOrder();
        if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE){
            $order->sendCompletedOrderEmail();
        }
        if($order->getState() == Mage_Sales_Model_Order::STATE_CANCELED){
            $order->sendCancelledOrderEmail();
        }
    }

    public function updateCommentOrder(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $_comment = Mage::app()->getRequest()->getParam('ordercomment');
        if(is_array($_comment) && isset($_comment['comment'])){
            $_orderComment =  trim($_comment['comment']);
            if(!empty($_orderComment)){
                $order->setCustomerComment($_orderComment);
            }
        }
    }

}