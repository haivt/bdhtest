<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 4:53 PM
 */

Class SM_Order_Model_Orders_Python extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_order/orders_python');
    }

    public function getShippingName()
    {
        return $this->getShippingDetailFirstName() . ' ' . $this->getShippingDetailLastName();
    }

    public function getBillingName()
    {
        return $this->getBillingDetailFirstName() . ' ' . $this->getBillingDetailLastName();
    }

    public function getStatusLabel(){
        if($this->getStatus() === '2') {
            return 'Completed';
        } else {
            return 'In-progress';
        }
    }

    /**
     * Retrieve order-items
     *
     * @return $items
     */
    public function getItems()
    {
        $query = 'SELECT * FROM ' . '`sm_orders_python_items`' . ' WHERE `order_id` = '
            . (int)$this->getId();
        $items = Mage::helper('sm_order')->getOrders($query);
        return $items;
    }
}