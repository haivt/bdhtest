<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 5/6/14
 * Time: 8:33 PM
 */
class SM_Order_Model_Resource_Sales_Order_Collection extends Mage_Sales_Model_Resource_Order_Collection
{
    public function getSize()
    {
        if (Mage::getSingleton('core/session')->getNumberNewItem() !== null) {
            return (int)parent::getSize() + (int)Mage::getSingleton('core/session')->getNumberNewItem();
        }
        return parent::getSize();
    }
}