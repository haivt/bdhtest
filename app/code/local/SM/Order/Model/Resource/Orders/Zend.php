<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 4:53 PM
 */

Class SM_Order_Model_Resource_Orders_Zend extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_order/orders_zend', 'id');
    }
}