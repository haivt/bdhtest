<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/23/14
 * Time: 4:35 PM
 */

class SM_Order_Block_Orders_History extends Mage_Sales_Block_Order_History
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('sales/order/history.phtml');

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc')
        ;

        $oldOrders = Mage::getResourceModel('sm_order/orders_python_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('user_id', Mage::getSingleton('customer/session')->getCustomer()->getOldId())
            ->setOrder('time', 'desc')
        ;

        $newCollection = Mage::helper('sm_order')->mergeOrders($orders, $oldOrders);
        $this->setOrders($newCollection);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getViewUrl($order)
    {
        if ($order->getIsOld() === true) {
            return $this->getUrl('sm_order/orders/view', array('order_id' => $order->getOldId()));
        }
        return $this->getUrl('*/*/view', array('order_id' => $order->getId()));
    }

    public function getTrackUrl($order)
    {
        return $this->getUrl('*/*/track', array('order_id' => $order->getId()));
    }

    public function getReorderUrl($order)
    {
        return $this->getUrl('*/*/reorder', array('order_id' => $order->getId()));
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
