<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/24/14
 * Time: 2:33 PM
 */

class SM_Order_Block_Orders_View extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('sm/orders/view.phtml');
    }

    protected function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('Order # %s', $this->getOrder()->getId()));
        }
    }

    /**
     * Retrieve current order model instance
     *
     * @return SM_Order_Model_Orders_Python
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * Return back url for logged in and guest users
     *
     * @return string
     */
    public function getBackUrl()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getUrl('*/*/history');
        }
        return Mage::getUrl('*/*/form');
    }

}