<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 2:53 PM
 */

class SM_Order_Block_Adminhtml_Orders_Python extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_orders_python';
        $this->_blockGroup = 'sm_order';
        $this->_headerText = Mage::helper('sm_order')->__("Manager Python's Order");
        parent::__construct();
        $this->_removeButton('add');
    }
}
