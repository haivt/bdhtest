<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 2:58 PM
 */

class SM_Order_Block_Adminhtml_Orders_Zend_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('orderGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sm_order/orders_zend')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sm_order')->__('Order ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'id',
        ));

        $this->addColumn('customers_name', array(
            'header' => Mage::helper('sm_order')->__('Customer Name'),
            'align' => 'left',
            'index' => 'customers_name',
            'width' => '150px',
        ));

        $this->addColumn('customers_email_address', array(
            'header' => Mage::helper('sm_order')->__('Customer Email'),
            'width' => '150px',
            'index' => 'customers_email_address',
        ));

        $this->addColumn('customers_postcode', array(
            'header' => Mage::helper('sm_order')->__('Customer Postcode'),
            'width' => '50px',
            'index' => 'customers_postcode',
        ));

        $this->addColumn('customers_state', array(
            'header' => Mage::helper('sm_order')->__('Customer State'),
            'width' => '90px',
            'align' => 'center',
            'index' => 'customers_state',
        ));

        $this->addColumn('customers_country', array(
            'header' => Mage::helper('sm_order')->__('Customer Country'),
            'align' => 'left',
            'index' => 'customers_country',
            'width' => '90px',
        ));

        $this->addColumn('customers_telephone', array(
            'header' => Mage::helper('sm_order')->__('Telephone'),
            'width' => '90px',
            'index' => 'customers_telephone',
        ));

        $this->addColumn('date_purchased', array(
            'header' => Mage::helper('sm_order')->__('Date Purchased'),
            'index' => 'date_purchased',
            'type' => 'datetime',
            'width' => '150px',
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sm_order')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sm_order')->__('XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }
}
