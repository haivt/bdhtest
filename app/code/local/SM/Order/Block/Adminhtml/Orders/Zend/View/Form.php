<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 7:32 PM
 */

class SM_Order_Block_Adminhtml_Orders_Zend_View_Form extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('sm/orders/zend/view/form.phtml');
    }
}
