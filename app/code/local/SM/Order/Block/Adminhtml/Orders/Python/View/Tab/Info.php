<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/11/14
 * Time: 4:00 PM
 */

class SM_Order_Block_Adminhtml_Orders_Python_View_Tab_Info
    extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Retrieve order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('python_order');
    }

    /**
     * Retrieve order-items
     *
     * @return $items
     */
    public function getItems()
    {
        $query = 'SELECT * FROM ' . '`sm_orders_python_items`' . ' WHERE `order_id` = '
            . (int)$this->getOrder()->getId();
        $items = Mage::helper('sm_order')->getOrders($query);
        return $items;
    }

    /**
     * ######################## TAB settings #################################
     */
    public function getTabLabel()
    {
        return Mage::helper('sm_order')->__('Information');
    }

    public function getTabTitle()
    {
        return Mage::helper('sm_order')->__('Order Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}