<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 7:29 PM
 */

class SM_Order_Block_Adminhtml_Orders_Python_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'sm_order';
        $this->_controller = 'adminhtml_orders_python';
        $this->_mode = 'view';

        parent::__construct();

        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('save');
        $this->setId('sm_orders_python_view');

    }

    /**
     * Retrieve order model object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('python_order');
    }

    public function getHeaderText()
    {
        $orderId = $this->getOrder()->getId();
        return Mage::helper('sales')->__('Order ID %s | %s', $orderId, $this->formatDate($this->getOrder()->getTime(), 'medium', true));
    }
}
