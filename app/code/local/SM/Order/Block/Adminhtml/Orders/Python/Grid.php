<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 2:58 PM
 */

class SM_Order_Block_Adminhtml_Orders_Python_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('orderGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sm_order/orders_python')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sm_order')->__('Order ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'id',
        ));

        $this->addColumn('billing_detail_first_name', array(
            'header' => Mage::helper('sm_order')->__('First Name of Billing'),
            'align' => 'left',
            'index' => 'billing_detail_first_name',
            'width' => '90px',
        ));

        $this->addColumn('billing_detail_last_name', array(
            'header' => Mage::helper('sm_order')->__('Last Name of Billing'),
            'width' => '90px',
            'index' => 'billing_detail_last_name',
        ));

        $this->addColumn('billing_detail_email', array(
            'header' => Mage::helper('sm_order')->__('Email of Billing'),
            'width' => '200px',
            'index' => 'billing_detail_email',
        ));

        $this->addColumn('billing_detail_country', array(
            'header' => Mage::helper('sm_order')->__('Country of Billing'),
            'width' => '90px',
            'align' => 'center',
            'index' => 'billing_detail_country',
        ));

        $this->addColumn('shipping_detail_first_name', array(
            'header' => Mage::helper('sm_order')->__('First Name of Shipping'),
            'align' => 'left',
            'index' => 'shipping_detail_first_name',
            'width' => '90px',
        ));

        $this->addColumn('shipping_detail_last_name', array(
            'header' => Mage::helper('sm_order')->__('Last Name of Shipping'),
            'width' => '90px',
            'index' => 'shipping_detail_last_name',
        ));

        $this->addColumn('total', array(
            'header' => Mage::helper('sm_order')->__('Total'),
            'width' => '50px',
            'index' => 'total',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sm_order')->__('Status'),
            'align' => 'left',
            'width' => '20px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'In-progress',
                2 => 'Completed',
            ),
        ));

        $this->addColumn('time', array(
            'header' => Mage::helper('sm_order')->__('Date Time'),
            'index' => 'time',
            'type' => 'datetime',
            'width' => '130px',
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sm_order')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sm_order')->__('XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }
}
