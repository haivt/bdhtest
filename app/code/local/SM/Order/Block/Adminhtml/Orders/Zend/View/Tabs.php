<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 7:38 PM
 */

class SM_Order_Block_Adminhtml_Orders_Zend_View_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sm_orders_zend_view_tabs');
        $this->setDestElementId('sm_orders_zend_view');
        $this->setTitle(Mage::helper('sm_order')->__('Order Information'));
    }
}
