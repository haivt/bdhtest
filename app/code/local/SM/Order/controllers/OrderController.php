<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Giga
 * Date: 4/26/14
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
class SM_Order_OrderController extends Mage_Core_Controller_Front_Action {

    /**
     * Customer order history
     */
    public function historyAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Orders | Bella Dream Hair'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }
}