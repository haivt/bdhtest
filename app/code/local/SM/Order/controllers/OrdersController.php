<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 3:17 PM
 */

class SM_Order_OrdersController extends Mage_Core_Controller_Front_Action
{

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Show list of old orders
     *
     */

    public function historyAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('Orders History | Bella Dream Hair'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

    /**
     * Init layout, messages and set active block for customer
     *
     * @return null
     */
    public function viewAction()
    {
        if (!$this->getRequest()->getParam('order_id')) {
            $this->_redirect('*/*/history');
            return;
        }

        $orders = Mage::getModel('sm_order/orders_python')->load($this->getRequest()->getParam('order_id'));
        if (!$orders->getId()) {
            $this->_redirect('*/*/history');
            return;
        }
        Mage::register('current_order', $orders);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('sm_order/orders/history');
        }
        $this->renderLayout();
    }
}