<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 3:17 PM
 */

class SM_Order_Adminhtml_Sm_Orders_PythonController extends Mage_Adminhtml_Controller_action
{
    /**
     * Initialize order model instance
     *
     * @return Sm_Order_Model_Orders_Python || false
     */
    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('id');
        $order = Mage::getModel('sm_order/orders_python')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->_redirect('*/*/');
            return false;
        }
        Mage::register('python_order', $order);
        return $order;
    }

    /**
     * Init layout, menu and breadcrumb
     *
     * @return SM_Order_Adminhtml_Sm_Orders_PythonController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/sm_orders')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Python Orders'), $this->__('Python Orders'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Python'))->_title($this->__('Orders'));
        $this->_initAction()
            ->renderLayout();
    }

    public function viewAction()
    {
        $this->_title($this->__('Python'))->_title($this->__('Orders'));
        $this->_initOrder();
        $this->_initAction();
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Order-Python.csv';
        $content = $this->getLayout()->createBlock('sm_order/adminhtml_orders_python_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'Order-Python.xml';
        $content = $this->getLayout()->createBlock('sm_order/adminhtml_orders_python_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('sm_order/adminhtml_orders_python_grid')->toHtml()
        );
    }
}