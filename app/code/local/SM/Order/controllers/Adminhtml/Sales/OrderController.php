<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Giga
 * Date: 4/22/14
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';


class SM_Order_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController {
    /**
     * Cancel order
     */
    public function cancelAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $order->cancel()
                    ->save();
                $this->_getSession()->addSuccess(
                    $this->__('The order has been cancelled.')
                );
            }
            catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addError($this->__('The order has not been cancelled.'));
                Mage::logException($e);
            }

            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        }
    }
}