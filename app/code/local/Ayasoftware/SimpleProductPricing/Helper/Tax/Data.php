<?php
class Ayasoftware_SimpleProductPricing_Helper_Tax_Data extends Mage_Tax_Helper_Data
{
public function getPrice($product, $price, $includingTax = null, $shippingAddress = null, $billingAddress = null,
	$ctc = null, $store = null, $priceIncludesTax = null, $roundPrice = true) 	{
		if(Mage::app()->getRequest()->getControllerName() != 'product' &&  $product->getTypeId() == 'configurable'){
			$prices = Mage::helper('spp')->getCheapestChildPrice($product);
			$product->setPrice($prices['Min']['price']);
			$product->setFinalPrice($prices['Min']['finalPrice']);
		}
		$productPrice = parent::getPrice($product, $price, $includingTax = null, $shippingAddress = null, $billingAddress = null,
		$ctc = null, $store = null, $priceIncludesTax = null, $roundPrice = true);
		return $productPrice;

	}
}
		