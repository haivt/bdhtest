<?php

/**
 * Product:       Xtento_OrderExport (1.4.3)
 * ID:            hvRjY0/5Yx1gfBGs5MimLVzHLgGpCSEhZf9vJlkmzUY=
 * Packaged:      2014-05-16T05:31:41+00:00
 * Last Modified: 2013-02-10T15:47:14+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Mysql4/Log/Collection.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Model_Mysql4_Log_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('xtento_orderexport/log');
    }
}