<?php

/**
 * Product:       Xtento_OrderExport (1.4.3)
 * ID:            hvRjY0/5Yx1gfBGs5MimLVzHLgGpCSEhZf9vJlkmzUY=
 * Packaged:      2014-05-16T05:31:41+00:00
 * Last Modified: 2012-11-18T20:56:11+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Export/Data/Interface.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

interface Xtento_OrderExport_Model_Export_Data_Interface {
    public function getExportData($entityType, $collectionItem);
    public function getConfiguration();
}